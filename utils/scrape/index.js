const { garmin } = require("./garmin");
const { strava } = require("./strava");

module.exports = {
  garmin,
  strava,
};
