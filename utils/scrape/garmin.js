const axios = require("axios");

exports.garmin = async (link) => {
  let id = "";
  if (link.split("/")[5].indexOf("?") >= 0) {
    id = link.split("/")[5].split("?")[0];
  } else {
    id = link.split("/")[5];
  }

  const { data } = await axios.get(
    `https://connect.garmin.com/modern/proxy/activity-service/activity/${id}`
  );

  const result = {
    run_tittle: data.activityName,
    run_date: data.summaryDTO.startTimeLocal,
    distance_m: data.summaryDTO.distance,
    time_s: data.summaryDTO.duration,
    pace_s: 1000 / data.summaryDTO.averageSpeed,
    elevation_m: data.summaryDTO.elevationGain,
  };
  return result;
};
