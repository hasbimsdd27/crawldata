const axios = require("axios");

exports.strava = async (link) => {
  const Months = [
    "january",
    "february",
    "march",
    "april",
    "may",
    "june",
    "july",
    "august",
    "september",
    "october",
    "november",
    "december",
  ];
  const { data } = await axios.get(link);
  const json = JSON.parse(
    data
      .split('-props="')[1]
      .split('">')[0]
      .replace(/&quot;/g, `"`)
  );
  const date = `${json.activity.date.split(",")[1].replace(" ", "")}-${String(
    Months.indexOf(
      json.activity.date.split(",")[0].split(" ")[0].toLowerCase()
    ) + 1
  ).padStart(2, "0")}-${
    json.activity.date.split(",")[0].split(" ")[1]
  }T00:00:00.0`;
  const result = {
    run_tittle: json.activity.name,
    run_date: date,
    distance_m: Number(json.activity.distance.split(" ")[0]) * 1000,
    time_s:
      Number(json.activity.time.split(":")[0]) * 60 +
      Number(json.activity.time.split(":")[1]),
    pace_s:
      1000 /
      ((Number(json.activity.distance.split(" ")[0]) * 1000) /
        (Number(json.activity.time.split(":")[0]) * 60 +
          Number(json.activity.time.split(":")[1]))),

    elevation_m: Number(json.activity.elevation.split(" ")[0]),
  };
  return result;
};
