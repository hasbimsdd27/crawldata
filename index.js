const express = require("express");
const app = express();
const port = 5000;
const cors = require("cors");
const route = require("./routes");

app.use(cors());
app.use(express.json());
app.use(route);

app.listen(port, () => console.log(`Server run on port ${port}`));
