'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('virtualruns', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      athlete_id: {
        type: Sequelize.INTEGER
      },
      run_tittle: {
        type: Sequelize.STRING
      },
      run_date: {
        type: Sequelize.DATE
      },
      distance_m: {
        type: Sequelize.INTEGER
      },
      time_s: {
        type: Sequelize.INTEGER
      },
      pace_s: {
        type: Sequelize.INTEGER
      },
      elevation_m: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('virtualruns');
  }
};