const models = require("../models");
const VR = models.virtualrun;
const Scrape = require("../utils/scrape");
const { ConvertSecond } = require("../utils/time");

const SaveData = async (data) => {
  const OldData = await VR.findOne({ where: { athlete_id: data.athlete_id } });
  if (OldData) {
    newData = { ...data, updatedAt: new Date() };
    VR.update(
      { ...OldData, newData },
      { where: { athlete_id: data.athlete_id } }
    );
  } else {
    newData = { ...data, createdAt: new Date(), updatedAt: new Date() };
    VR.create({
      ...newData,
    });
  }
};

exports.GetData = async (req, res) => {
  try {
    const StartDate = new Date("2020-06-18T00:00:00");
    const { link } = req.body;
    const Web = link.split("/")[2];
    if (Web === "connect.garmin.com") {
      result = await Scrape.garmin(link);
      RunDate = new Date(result.run_date);
      if (RunDate < StartDate) {
        throw 2;
      } else {
        // await SaveData({ ...result, athlete_id });
        res.send({
          status: true,
          message: "get data success",
          data: {
            tittle: result.tittle,
            run_date: {
              date: result.run_date.split("T")[0],
              time: result.run_date.split("T")[1],
            },
            distance: result.distance_m.toFixed(2),
            time: result.time_s.toFixed(2),
            pace: result.pace_s.toFixed(2),
            elevation: Math.round(result.elevation_m),
          },
        });
      }
    } else if (Web === "www.strava.com") {
      result = await Scrape.strava(link);
      RunDate = new Date(result.run_date);
      if (RunDate < StartDate) {
        throw 2;
      } else {
        // await SaveData({ ...result, athlete_id });
        res.send({
          status: true,
          message: "get data success",
          data: {
            tittle: result.tittle,
            run_date: {
              date: result.run_date.split("T")[0],
              time: result.run_date.split("T")[1],
            },
            distance: result.distance_m.toFixed(2),
            time: result.time_s.toFixed(2),
            pace: result.pace_s.toFixed(2),
            elevation: Math.round(result.elevation_m),
          },
        });
      }
    } else {
      throw 1;
    }
  } catch (err) {
    switch (err) {
      case 1:
        res.status(400).send({ status: false, message: "link not supported" });
        break;
      case 2:
        res.status(400).send({
          status: false,
          message: "your data generated before event started",
        });
        break;
      default:
        // res.status(400).send({ status: false, message: "some error occured" });
        res
          .status(err.response.status)
          .send({ status: false, message: err.response.statusText });
        console.log(err);
        break;
    }
  }
};
