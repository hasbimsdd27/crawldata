const express = require("express");
const router = express.Router();

const { GetData } = require("../controllers/GetData");

router.post("/", GetData);

module.exports = router;
