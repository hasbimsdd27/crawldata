'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class virtualrun extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  virtualrun.init({
    athlete_id: DataTypes.INTEGER,
    run_tittle: DataTypes.STRING,
    run_date: DataTypes.DATE,
    distance_m: DataTypes.INTEGER,
    time_s: DataTypes.INTEGER,
    pace_s: DataTypes.INTEGER,
    elevation_m: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'virtualrun',
  });
  return virtualrun;
};